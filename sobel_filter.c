#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

// sobel filter処理する関数
void sobel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
             Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }

  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      // sobel filter処理
      sobel(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void sobel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
             Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  char deltaX[3][3] = {{ 1,  0, -1},
                       { 2,  0, -2},
                       { 1,  0, -1}};

  char deltaY[3][3] = {{ 1,  2,  1},
                       { 0,  0,  0},
                       {-1, -2, -1}};

  int    x, y, i, j; // for文用の変数
  int    color;
  double sumX, sumY; // 近傍画素の合計値
  double slope;      // 勾配

  for (color = 0; color < 3; color++) {
    for (y = 0; y < MAXHEIGHT; y++) {
      for (x = 0; x < MAXWIDTH; x++) {
        slope = 0;
        sumX  = 0;
        sumY  = 0;
        // 空間フィルタ処理
        for (i = -1; i < 2; i++) {
          for (j = -1; j < 2; j++) {
            // 画像の端だったらスキップ
            if (y+i < 256 && y+i >=0 && x+j < 256 && x+j >=0 ){
              sumX += deltaX[i+1][j+1] * source[color][y+i][x+j];
              sumY += deltaY[i+1][j+1] * source[color][y+i][x+j];
            }
          }
        }
        // 勾配の計算
        slope = sqrt( pow(sumX,2) + pow(sumY,2) );
        if(slope > 255) slope = 255;
        // 画像に格納
        results[color][y][x] = slope;
      }
    }
  }
}
