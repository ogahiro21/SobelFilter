# Soblel Filter
入力画像をSobelフィルタ処理をする。

## What's Sobel filter
Soble(ソーベル)は、画像から輪郭を抽出する空間フィルタの一つ。  
- 入力画像  
![in](https://gitlab.com/ogahiro21/SobelFilter/raw/image/image/in12.jpeg)
- 出力画像  
![out](https://gitlab.com/ogahiro21/SobelFilter/raw/image/image/out12.jpeg) <br>
「注目画素との距離に応じて重み付けを変化させた」ものがソーベルフィルタです。  
$`\Delta_x = \begin{pmatrix} 1 & 0 & -1 \\ 2 & 0 & -2 \\ 1 & 0 & -1 \end{pmatrix}`$
$`\Delta_y = \begin{pmatrix} 1 & 2 & 1 \\ 0 & 0 & 0 \\ -1 & -2 & -1 \end{pmatrix} `$  
とすることで勾配を計算できる。  
$`\Delta_x f = \Delta_x * `$ 近傍画素値  
$`\Delta_y f = \Delta_y * `$ 近傍画素値  
とした時の注目画素の更新値は  
$`g[i,j] = \sqrt{(\Delta_x f)^2 + (\Delta_y f)^2}`$  
と表すことができる。  
また、ここでは画像領域外の値は0として扱う。

## Usage
**コンパイル**
```
gcc -o cpbmp sobel_filter.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in12.bmp ans12.bmp
```
**正解画像と回答画像の比較**
```
/home/class/j5/imageSystem/bin/diffbmp out12.bmp ans12.bmp
```
